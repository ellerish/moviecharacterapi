﻿# Movie Character API
This is an assignment for the Noroff .NET upskill course winter 2021.
The application is an ASP.NET Core application that will manage characters, movies, and franchises: 
- REST API using Entity Framework 
- Uses SQL Server to manage a database
- EF Entities generate and manage a database schema
- Mange database data with EF DbContext
- REST endpoints and proper responses with ASP.NET Core.
- Documentation for REST api using Swagger/Open API

## Table of contents
- [Getting started](#getting-started) 
- [Maintainers](#maintainers)

## Getting started
- Clone the repository 
- Open in Visual Studio.
- Change "DefaultConnection" to your own in appsettings.json
- In Package Manager console run "Update-Database"
- Build application: Swagger/Open API documentation

## Maintainers
- Birger Topphol
- Elise Rishaug
- Melfyn Jenkins