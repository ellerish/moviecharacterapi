﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DB;
using MovieCharacterAPI.Models;
using AutoMapper;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Data;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharacterContext _context;
        private readonly IMapper _mapper;
        public MoviesController(MovieCharacterContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<CommonResponse<IEnumerable<MovieDTO>>>> GetMovies()
        {
            // Create response object
            CommonResponse<IEnumerable<MovieDTO>> respons = new CommonResponse<IEnumerable<MovieDTO>>();
            // Fetch list of model class and map to dto
            var modelMovies = await _context.Movies.ToListAsync();
            List<MovieDTO> movies = _mapper.Map<List<MovieDTO>>(modelMovies);
            // Return the data
            respons.Data = movies;
            return Ok(respons);
        }

        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CommonResponse<MovieDTO>>> GetMovie(int id)
        {
            // Create response object
            CommonResponse<MovieDTO> respons = new CommonResponse<MovieDTO>();
            var movieModel = await _context.Movies.FindAsync(id);
            if (movieModel == null)
            {
                respons.Error = new Error { Status = 404, Message = "Cannot find an movie with that Id" };
                return NotFound(respons);
            }
            // Map 
            respons.Data = _mapper.Map<MovieDTO>(movieModel);
            return Ok(respons);
        }

        // PUT: api/Movies/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieDTO movie)
        {
            // Create response object
            CommonResponse<MovieDTO> respons = new CommonResponse<MovieDTO>();
            if (id != movie.Id)
            {
                respons.Error = new Error { Status = 400, Message = "There was a mismatch with the provided id and the object." };
                return BadRequest(respons);
            }
            _context.Entry(_mapper.Map<Movie>(movie)).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        // POST: api/Movies
        [HttpPost]
        public async Task<ActionResult<CommonResponse<MovieDTO>>> PostMovie(MovieCreateDTO movie)
        {
            // Create response object
            CommonResponse<MovieDTO> respons = new CommonResponse<MovieDTO>();
            if (!ModelState.IsValid)
            {
                respons.Error = new Error
                {
                    Status = 400,
                    Message = "The movie did not pass validation, ensure it is in the correct format."
                };
                return BadRequest(respons);
            }
            Movie movieModel = _mapper.Map<Movie>(movie);
            // Try catch
            try
            {
                _context.Movies.Add(movieModel);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                respons.Error = new Error { Status = 500, Message = e.Message };
                return StatusCode(StatusCodes.Status500InternalServerError, respons);
            }
            // Map to MovieDTO 
            respons.Data = _mapper.Map<MovieDTO>(movieModel);
            return CreatedAtAction("GetMovie", new { id = respons.Data.Id }, respons);
        }

        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CommonResponse<MovieDTO>>> DeleteMovie(int id)
        {
            // Make response object
            CommonResponse<MovieDTO> respons = new CommonResponse<MovieDTO>();

            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                respons.Error = new Error { Status = 404, Message = "An movie with that id could not be found." };
                return NotFound(respons);
            }
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            // Map model class to dto 
            respons.Data = _mapper.Map<MovieDTO>(movie);
            return Ok(respons);
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        //Get Characters in Movie 
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<CommonResponse<CharacterDTO>>> GetCharacatersInMovie(int id)
        {
            // Make response object
            CommonResponse<IEnumerable<CharacterDTO>> respons = new CommonResponse<IEnumerable<CharacterDTO>>();
            Movie movie = await _context.Movies.Include(c => c.Characters).Where(m => m.Id == id).FirstOrDefaultAsync();
            if (movie == null)
            {
                respons.Error = new Error { Status = 404, Message = "An movie with that id could not be found." };
                return NotFound(respons);
            }
            foreach (Character character in movie.Characters)
            {
                character.Movies = null;
            }
            // Map to dto
            respons.Data = _mapper.Map<List<CharacterDTO>>(movie.Characters);
            return Ok(respons);
        }
     
    }
}
