using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Data;
using MovieCharacterAPI.DB;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieCharacterContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<CommonResponse<IEnumerable<FranchiseDTO>>>> GetFranchises()
        {
            CommonResponse<IEnumerable<FranchiseDTO>> response = new CommonResponse<IEnumerable<FranchiseDTO>>();
            // Maps from model to DTO
            var modelFranchises = await _context.Franchises.ToListAsync();
            List<FranchiseDTO> franchises = _mapper.Map<List<FranchiseDTO>>(modelFranchises);

            // Return data
            response.Data = franchises;
            return Ok(response);
        }

        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CommonResponse<FranchiseDTO>>> GetFranchise(int id)
        {
            CommonResponse<FranchiseDTO> response = new CommonResponse<FranchiseDTO>();

            var modelFranchise = await _context.Franchises.FindAsync(id);
            
            if (modelFranchise == null)
            {
                response.Error = new Error { Status = 404, Message = "Cannot find an franchise with that Id" };
                return NotFound(response);
            }
            // Map to DTO
            response.Data = _mapper.Map<FranchiseDTO>(modelFranchise);

            return Ok(response);
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseDTO franchise)
        {
            CommonResponse<FranchiseDTO> response = new CommonResponse<FranchiseDTO>();

            if (id != franchise.Id)
            {
                response.Error = new Error { Status = 400, Message = "There was a mismatch with the provided id and the object." };
                return BadRequest(response);
            }
            // Maps to franchise model
            Franchise franchiseModel = _mapper.Map<Franchise>(franchise);
            _context.Entry(franchiseModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CommonResponse<FranchiseDTO>>> PostFranchise(FranchiseCreateDTO franchise)
        {
            CommonResponse<FranchiseDTO> response = new CommonResponse<FranchiseDTO>();

            if (!ModelState.IsValid)
            {
                response.Error = new Error
                {
                    Status = 400,
                    Message = "The author did not pass validation, ensure it is in the correct format."
                };
                return BadRequest(response);
            }

            Franchise franchiseModel = _mapper.Map<Franchise>(franchise);
            
            try
            {
                _context.Franchises.Add(franchiseModel);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                response.Error = new Error { Status = 500, Message = e.Message };
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }           
            // Map to franchiseDTO
            response.Data = _mapper.Map<FranchiseDTO>(franchiseModel);

            return CreatedAtAction("GetFranchise", new { id = response.Data.Id}, response);
        }

        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CommonResponse<FranchiseDTO>>> DeleteCharacter(int id)
        {
            CommonResponse<FranchiseDTO> response = new CommonResponse<FranchiseDTO>();

            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                response.Error = new Error { Status = 404, Message = "A franchise with that id could not be found." };
                return NotFound(response);
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            //Map to franchiseDto
            response.Data = _mapper.Map<FranchiseDTO>(franchise);
            return Ok(response);
        }

        // Get all movies in a franchise
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMoviesInFranchise(int id)
        {
            Franchise franchise = await _context.Franchises.Include(f => f.Movies).Where(f => f.Id == id).FirstOrDefaultAsync();
            // Maps from model to DTO
            List<MovieDTO> movies = _mapper.Map<List<MovieDTO>>(franchise.Movies);

            if (movies == null)
            {
                return NotFound();
            }

            return Ok(movies);
        }

        // Get all characters in a franchise
        // GET: api/Franchises/5/Characters
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<CommonResponse<IEnumerable<CharacterDTO>>>> GetFranchiseCharacters(int id)
        {
            CommonResponse<IEnumerable<CharacterDTO>> respons = new();

            var characters = await _context.Franchises
                .Where(f => f.Id == id)
                .SelectMany(f => f.Movies)
                .SelectMany(m => m.Characters)
                .Distinct()
                .ToListAsync();

            respons.Data = _mapper.Map<List<CharacterDTO>>(characters);

            return Ok(respons);
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
