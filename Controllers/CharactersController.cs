﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Data;
using MovieCharacterAPI.DB;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharacterContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<CommonResponse<IEnumerable<CharacterDTO>>>> GetCharacters()
        {
            CommonResponse<IEnumerable<CharacterDTO>> respons = new();

            var characters = await _context.Characters.ToListAsync();
            respons.Data = _mapper.Map<List<CharacterDTO>>(characters);

            return Ok(respons);
        }

        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CommonResponse<CharacterDTO>>> GetCharacter(int id)
        {
            CommonResponse<CharacterDTO> respons = new();

            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            respons.Data = _mapper.Map<CharacterDTO>(character);

            return Ok(respons);
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterDTO character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _context.Entry(_mapper.Map<Character>(character)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CommonResponse<CharacterDTO>>> PostCharacter(CharacterCreateDTO character)
        {
            CommonResponse<CharacterDTO> respons = new();
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Character characterModel = _mapper.Map<Character>(character);

            try
            {
                _context.Characters.Add(characterModel);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }

            respons.Data = _mapper.Map<CharacterDTO>(characterModel);

            return CreatedAtAction(nameof(GetCharacter), new { id = respons.Data.Id }, respons);
        }

        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CommonResponse<CharacterDTO>>> DeleteCharacter(int id)
        {
            CommonResponse<CharacterDTO> respons = new();

            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            respons.Data = _mapper.Map<CharacterDTO>(character);

            return Ok(respons);
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
