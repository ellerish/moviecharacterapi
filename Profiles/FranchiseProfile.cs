﻿using AutoMapper;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<FranchiseDTO, Franchise>().ReverseMap();
            CreateMap<FranchiseCreateDTO, Franchise>().ReverseMap();
        }

    }
}
