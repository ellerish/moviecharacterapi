﻿using System;
using AutoMapper;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Controllers
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<MovieDTO, Movie>().ReverseMap();
            CreateMap<MovieCreateDTO, Movie>().ReverseMap();

        }
    }
}
