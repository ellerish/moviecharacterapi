﻿using AutoMapper;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDTO>().ReverseMap();
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();
            CreateMap<CharacterDTO, CharacterCreateDTO>().ReverseMap();
        }
    }
}
