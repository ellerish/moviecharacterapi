﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.DB
{
    public class MovieCharacterContext : DbContext 
    {
        public MovieCharacterContext(DbContextOptions<MovieCharacterContext> options) : base(options)
        {
        }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Franchises
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Star Wars", Description = "It was produced by Lucasfilm and distributed by 20th Century Fox, and consists of the original Star Wars film (1977), The Empire Strikes Back (1980) and Return of the Jedi (1983). Beginning in medias res, the original trilogy serves as the second act of the nine-episode Skywalker saga" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "Star Trek", Description = "It followed the voyages of the starship USS Enterprise, a space exploration vessel built by the United Federation of Planets in the 23rd century, on a mission \"to explore strange new worlds, to seek out new life and new civilizations, to boldly go where no man has gone before\"" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 3, Name = "Toy Story", Description = "The franchise is based on the anthropomorphic concept that all toys, unknown to humans, are secretly alive and the films focus on a diverse group of toys that feature a classic cowboy doll named Sheriff Woody and a modern spaceman action figure named Buzz Lightyear. The group unexpectedly embark on adventures that challenge and change them" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 4, Name = "James Bond", Description = "The James Bond film series is a series of spy films based on the fictional character of MI6 agent James Bond, \"007\", who originally appeared in a series of books by Ian Fleming." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 5, Name = "Shrek", Description = "The series primarily focuses on Shrek, a bad-tempered ogre, who begrudgingly accepts a quest to rescue a princess, resulting in him finding friends and going on many subsequent adventures in a fairy tale world." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 6, Name = "Die Hard", Description = "Die Hard is an American action film series that originated with Roderick Thorp's novel Nothing Lasts Forever. All five films revolve around the main character of John McClane, a New York City/Los Angeles police detective who continually finds himself in the middle of a Christmas crisis and intrigues where he is the only hope against disaster." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 7, Name = "Rambo", Description = "The films follow John Rambo, a United States Army Special Forces veteran played by Sylvester Stallone, whose experience fighting in the Vietnam War traumatized him but also gave him superior military skills, which he has used to fight corrupt police officers, enemy troops and drug cartels." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 8, Name = "John Wick", Description = "Keanu Reeves plays John Wick, a retired hitman seeking vengeance for the killing of the dog given to him by his recently deceased wife, and for stealing his car." });


            // Movies
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 1, Title = "A New Hope", Genre = "Action, Adventure, Fantasy", ReleaseYear = 1977, Director = "George Lucas", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/8/87/StarWarsMoviePoster1977.jpg", TrailerUrl = "https://www.youtube.com/watch?v=1g3_CFmnU7k", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 2, Title = "The Empire Strikes Back", Genre = "Action, Adventure, Fantasy", ReleaseYear = 1980, Director = "Irvin Kershner", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/3/3c/SW_-_Empire_Strikes_Back.jpg", TrailerUrl = "https://www.youtube.com/watch?v=mz_YWNhKOkM", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 3, Title = "Return of the Jedi", Genre = "Action, Adventure, Fantasy", ReleaseYear = 1983, Director = "Richard Marquand", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/b/b2/ReturnOfTheJediPoster1983.jpg", TrailerUrl = "https://www.youtube.com/watch?v=5UfA_aKBGMc", FranchiseId = 1 });

            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 4, Title = "Star Trek: The Motion Picture", Genre = "Sci-fi", ReleaseYear = 1979, Director = "Robert Wise", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/d/df/Star_Trek_The_Motion_Picture_poster.png", TrailerUrl = "https://www.youtube.com/watch?v=wbvxV2OJQKk&ab_channel=MovieclipsClassicTrailers", FranchiseId = 2 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 5, Title = "Star Trek II: The Wrath of Khan", Genre = "Sci-fi", ReleaseYear = 1982, Director = "Nicholas Meyer", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/9/9a/Star_Trek_II_The_Wrath_of_Khan.png", TrailerUrl = "https://www.youtube.com/watch?v=x8X44NRltMM&ab_channel=MovieclipsClassicTrailers", FranchiseId = 2 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 6, Title = "Star Trek III: The Search for Spock", Genre = "Sci-fi", ReleaseYear = 1984, Director = "Harve Bennett", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/b/b6/Star_Trek_III_The_Search_for_Spock.png", TrailerUrl = "https://www.youtube.com/watch?v=aCgWzlxnqhc&ab_channel=MovieclipsClassicTrailers", FranchiseId = 2 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 7, Title = "Star Trek: First Contact", Genre = "Sci-fi", ReleaseYear = 1996, Director = "Jonathan Frakes", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/0/01/Star_trek_first_contact_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=B3NJ49VyjDs&ab_channel=MovieclipsClassicTrailers", FranchiseId = 2 });

            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 8, Title = "Toy Story", Genre = "Animation, Adventure, Comedy", ReleaseYear = 1995, Director = "John Lasseter", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/1/13/Toy_Story.jpg", TrailerUrl = "https://www.youtube.com/watch?v=rNk1Wi8SvNc", FranchiseId = 3 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 9, Title = "Toy Story 2", Genre = "Animation, Adventure, Comedy", ReleaseYear = 1999, Director = "John Lasseter, Ash Brannon, Lee Unkrich", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/c/c0/Toy_Story_2.jpg", TrailerUrl = "https://www.youtube.com/watch?v=xNWSGRD5CzU", FranchiseId = 3 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 10, Title = "Toy Story 3", Genre = "Animation, Adventure, Comedy", ReleaseYear = 2010, Director = "Lee Unkrich", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/6/69/Toy_Story_3_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=JcpWXaA2qeg", FranchiseId = 3 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 11, Title = "Toy Story 4", Genre = "Animation, Adventure, Comedy", ReleaseYear = 2019, Director = "Josh Cooley", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/4/4c/Toy_Story_4_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=wmiIUN-7qhE", FranchiseId = 3 });

            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 12, Title = "Dr. No", Genre = "Spy, Action", ReleaseYear = 1962, Director = "Terence Young", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/4/43/Dr._No_-_UK_cinema_poster.jpg", TrailerUrl = "", FranchiseId = 4 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 13, Title = "Goldeneye", Genre = "Spy, Action", ReleaseYear = 1995, Director = "Martin Campbell", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/2/24/GoldenEye_-_UK_cinema_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=lcOqUE0u1LM&ab_channel=TrailersPlaygroundHD", FranchiseId = 4 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 14, Title = "On Her Majesty's Secret Service", Genre = "Spy, Action", ReleaseYear = 1969, Director = "Peter R. Hunt", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/f/f3/On_Her_Majesty%27s_Secret_Service_-_UK_cinema_poster.jpg", TrailerUrl = "", FranchiseId = 4 });


            // Characters

            // Star Wars
            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, Fullname = "Luke Skywalker", Alias = "", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/9/9b/Luke_Skywalker.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 2, Fullname = "Han Solo", Alias = "", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/b/be/Han_Solo_depicted_in_promotional_image_for_Star_Wars_%281977%29.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 3, Fullname = "Princess Leia Organa", Alias = "", Gender = "Female", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/1/1b/Princess_Leia%27s_characteristic_hairstyle.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 4, Fullname = "Governor Wilhuff Tarkin", Alias = "Grand Moff Tarkin", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/5/5a/Grand_Moff_Tarkin.png/330px-Grand_Moff_Tarkin.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 5, Fullname = "Obi-Wan Kenobi", Alias = "Ben Kenobi", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/3/32/Ben_Kenobi.png/330px-Ben_Kenobi.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 6, Fullname = "C-3PO", Alias = "", Gender = "Droid", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/5/5c/C-3PO_droid.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 7, Fullname = "R2-D2", Alias = "Artoo", Gender = "Droid", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/3/39/R2-D2_Droid.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 8, Fullname = "Chewbacca", Alias = "Chewie", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/6/6d/Chewbacca-2-.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 9, Fullname = "Darth Vader", Alias = "", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/7/76/Darth_Vader.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 10, Fullname = "Baron Landonis Balthazar Calrissian III", Alias = "Lando", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/c/cb/Lando6-2.jpg/330px-Lando6-2.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 11, Fullname = "Yoda", Alias = "", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/9/9b/Yoda_Empire_Strikes_Back.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 12, Fullname = "Boba Fett", Alias = "", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/3/3e/FettbobaJB.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 13, Fullname = "Sheev Palpatine", Alias = "The Emperor, Darth Sidious", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/8/8f/Emperor_RotJ.png/330px-Emperor_RotJ.png" });

            // Star Trek
            modelBuilder.Entity<Character>().HasData(new Character { Id = 14, Fullname = "James T. Kirk", Alias = "Captain Kirk", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/a/a5/Star_Trek_William_Shatner.JPG" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 15, Fullname = "Spock", Alias = "", Gender = "Male", ImageUrl = "https://en.wikipedia.org/wiki/File:Leonard_Nimoy_as_Spock_1967.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 16, Fullname = "Dr. Leonard McCoy", Alias = "Bones", Gender = "Male", ImageUrl = "https://en.wikipedia.org/wiki/File:DeForest_Kelley,_Dr._McCoy,_Star_Trek.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 17, Fullname = "Montgomery Scott", Alias = "Scotty", Gender = "Male", ImageUrl = "https://en.wikipedia.org/wiki/File:James_Doohan_Scotty_Star_Trek.JPG" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 18, Fullname = "Nyota Uhura", Alias = "", Gender = "Female", ImageUrl = "https://en.wikipedia.org/wiki/File:Nichelle_Nichols,_NASA_Recruiter_-_GPN-2004-00017.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 19, Fullname = "Hikaru Sulu", Alias = "", Gender = "Male", ImageUrl = "https://en.wikipedia.org/wiki/File:George_Takei_Sulu_Star_Trek.JPG" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 20, Fullname = "Pavel Chekov", Alias = "", Gender = "Male", ImageUrl = "https://en.wikipedia.org/wiki/File:Walter_Koenig_Star_Trek.JPG" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 21, Fullname = "Christine Chapel", Alias = "", Gender = "Female", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/d/d8/Christine_Chapel_Promotional_Image.jpg" });

            modelBuilder.Entity<Character>().HasData(new Character { Id = 22, Fullname = "Jean-Luc Picard", Alias = "", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/8/8e/Patrick_Steward_as_Jean-Luc_Picard_in_1996%27s_Star_Trek_First_Contact.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 23, Fullname = "Riker", Alias = "", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/8/8e/Patrick_Steward_as_Jean-Luc_Picard_in_1996%27s_Star_Trek_First_Contact.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 24, Fullname = "Data", Alias = "", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/8/8e/Patrick_Steward_as_Jean-Luc_Picard_in_1996%27s_Star_Trek_First_Contact.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 25, Fullname = "Geordi", Alias = "", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/8/8e/Patrick_Steward_as_Jean-Luc_Picard_in_1996%27s_Star_Trek_First_Contact.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 26, Fullname = "Worf", Alias = "", Gender = "Male", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/8/8e/Patrick_Steward_as_Jean-Luc_Picard_in_1996%27s_Star_Trek_First_Contact.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 27, Fullname = "Beverly", Alias = "", Gender = "Female", ImageUrl = "https://upload.wikimedia.org/wikipedia/en/8/8e/Patrick_Steward_as_Jean-Luc_Picard_in_1996%27s_Star_Trek_First_Contact.jpg" });


            // Relationships

            // Star Wars: A New Hope
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 1, CharactersId = 1 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 1, CharactersId = 2 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 1, CharactersId = 3 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 1, CharactersId = 4 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 1, CharactersId = 5 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 1, CharactersId = 6 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 1, CharactersId = 7 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 1, CharactersId = 8 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 1, CharactersId = 9 });

            // Star Wars: The Empire Strikes Back
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 1 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 2 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 3 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 4 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 5 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 6 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 7 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 8 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 9 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 10 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 11 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 12 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 2, CharactersId = 13 });

            // Star Wars: The Return of the Jedi
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 1 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 2 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 3 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 4 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 5 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 6 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 7 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 8 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 9 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 10 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 11 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 3, CharactersId = 13 });

            // Star Trek: The Motion Picture
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 4, CharactersId = 14 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 4, CharactersId = 15 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 4, CharactersId = 16 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 4, CharactersId = 17 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 4, CharactersId = 18 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 4, CharactersId = 19 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 4, CharactersId = 20 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 4, CharactersId = 21 });

            // Star Trek II: The Wrath of Khan
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 5, CharactersId = 14 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 5, CharactersId = 15 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 5, CharactersId = 16 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 5, CharactersId = 17 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 5, CharactersId = 18 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 5, CharactersId = 19 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 5, CharactersId = 20 });

            // Star Trek III: The Search for Spock
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 6, CharactersId = 14 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 6, CharactersId = 15 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 6, CharactersId = 16 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 6, CharactersId = 17 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 6, CharactersId = 18 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 6, CharactersId = 19 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 6, CharactersId = 20 });

            // Star Trek: First Contact
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 7, CharactersId = 22 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 7, CharactersId = 23 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 7, CharactersId = 24 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 7, CharactersId = 25 });
            modelBuilder.Entity("CharacterMovie").HasData(new { MoviesId = 7, CharactersId = 26 });
        }
    }
}
