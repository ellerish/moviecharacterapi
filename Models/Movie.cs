﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models
{
    public class Movie
    {
        //Primary key
        public int Id { get; set; }

        //Fields
        [MaxLength(200)]
        [Required]
        public string Title { get; set; }

        [MaxLength(200)]
        public string Genre { get; set; }

        public int ReleaseYear { get; set; }

        [MaxLength(200)]
        public string Director { get; set; }

        [MaxLength(2083)]
        [DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; }

        [MaxLength(2083)]
        [DataType(DataType.Url)]
        public string TrailerUrl { get; set; }

        //Relationships
        public ICollection<Character> Characters { get; set; }
        public Franchise Franchise { get; set; }

        public int FranchiseId { get; set; }
    }
}
