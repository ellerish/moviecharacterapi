﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models
{
    public class Franchise
    {
        // Primary key
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
