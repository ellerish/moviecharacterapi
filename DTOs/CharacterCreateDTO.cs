﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models
{
    public class CharacterCreateDTO
    {
        // Fields
        [Required]
        [MaxLength(200)]
        public string Fullname { get; set; }

        [MaxLength(200)]
        public string Alias { get; set; }

        [MaxLength(50)]
        public string Gender { get; set; }

        [DataType(DataType.ImageUrl)]
        [MaxLength(2083)]
        public string ImageUrl { get; set; }
    }
}
