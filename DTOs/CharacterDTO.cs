﻿namespace MovieCharacterAPI.DTOs
{
    public class CharacterDTO
    {
        // Primary key
        public int Id { get; set; }

        // Fields
        public string Fullname { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string ImageUrl { get; set; }
    }
}
